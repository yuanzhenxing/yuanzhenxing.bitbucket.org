<?php 
/**
  Sample test endpoint. The actual api should be more secure than this. Of course!
**/

$domain = '*';
header('Access-Control-Allow-Origin: '.$domain);
header('Access-Control-Allow-Methods: GET, POST, PUT');
header('content-type: application/json; charset=utf-8');

function redirect($url, $statusCode = 303)
{
   header('Location: ' . $url, true, $statusCode);
   die();
}

$action = @$_POST['action'];

if ($action === 'SelectRegion') {
  include('../_popup-select-region.html');
}

?>