/* kp-global : 0.0.1 : Thu Aug 04 2016 15:26:44 GMT+0800 (中国标准时间) */(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var slice = [].slice;

$.kpFnName = function(fn) {
  return fn.name || (fn + '').split(/\s|\(/)[1];
};

$.kpInit = function(fn, name, set) {
  if (set == null) {
    set = true;
  }
  window.KP = window.KP || {};
  window.KP.apps = window.KP.apps || {};
  if (fn && set) {
    name = name || $.kpFnName(fn);
    return window.KP.apps[name] = fn;
  }
};

$.kpPluggin = function(fn, name, bypass, elPluggin) {
  var obj;
  if (bypass == null) {
    bypass = false;
  }
  if (elPluggin == null) {
    elPluggin = true;
  }
  obj = {};
  $.kpInit(fn, name, true);
  name = name || $.kpFnName(fn);
  obj[name] = function() {
    var args, option;
    option = arguments[0], args = 2 <= arguments.length ? slice.call(arguments, 1) : [];
    this.each(function() {
      var $this, data, key;
      $this = $(this);
      key = 'kp-' + name;
      data = $this.data(key);
      if (!data || bypass) {
        $this.data('name', name);
        $this.data(key, (data = new fn(option, this)));
      }
      if (typeof option === 'string') {
        return data[option].apply(data, args);
      }
    });
  };
  $.fn.extend(obj);
  if (!elPluggin) {
    obj[name] = function(option) {
      return new fn(option);
    };
    return $.extend(obj);
  }
};

$.kpAPI = function(key, value) {
  value = value ? value : KP.api;
  if (value) {
    if (value[key]) {
      return value[key];
    } else {
      return null;
    }
  } else {
    return null;
  }
};



},{}],2:[function(require,module,exports){
String.prototype.toBoolean = function() {
  return /^true$/i.test(this);
};

String.prototype.byId = function() {
  return $('#' + this);
};

String.prototype.byClass = function() {
  return $('.' + this);
};



},{}],3:[function(require,module,exports){
require('./helpers/common/kp-pluggin.coffee');

require('./helpers/common/string.prototype.coffee');

require('./modules/common/kp-module.coffee');

require('./modules/ada/common/ada-base.coffee');

require('./modules/common/tabs.coffee');

require('./modules/ada/_ada-region-select.coffee');

require('./modules/common/ajax-content.coffee');

require('./modules/ada/common/_ada-popup.coffee');

require('./modules/common/popup.coffee');

require('./modules/common/geolocation.coffee');

require('./modules/ada/common/_ada-hidden.coffee');

require('./modules/ada/_ada-header-menu.coffee');

require('./modules/ada/_ada-mobile-menu.coffee');

require('./modules/ada/common/_ada-keydown.coffee');

require('./modules/ada/common/_ada-tabs.coffee');

require('./modules/ada/common/_ada-navigation.coffee');

require('./modules/ada/_ada-image-viewer.coffee');

require('./modules/ada/ada.coffee');

require('./modules/common/footnote.coffee');

require('./modules/doc-ready.coffee');



},{"./helpers/common/kp-pluggin.coffee":1,"./helpers/common/string.prototype.coffee":2,"./modules/ada/_ada-header-menu.coffee":4,"./modules/ada/_ada-image-viewer.coffee":5,"./modules/ada/_ada-mobile-menu.coffee":6,"./modules/ada/_ada-region-select.coffee":7,"./modules/ada/ada.coffee":8,"./modules/ada/common/_ada-hidden.coffee":9,"./modules/ada/common/_ada-keydown.coffee":10,"./modules/ada/common/_ada-navigation.coffee":11,"./modules/ada/common/_ada-popup.coffee":12,"./modules/ada/common/_ada-tabs.coffee":13,"./modules/ada/common/ada-base.coffee":14,"./modules/common/ajax-content.coffee":15,"./modules/common/footnote.coffee":16,"./modules/common/geolocation.coffee":17,"./modules/common/kp-module.coffee":18,"./modules/common/popup.coffee":19,"./modules/common/tabs.coffee":20,"./modules/doc-ready.coffee":21}],4:[function(require,module,exports){
var ADAHeaderMenu,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

ADAHeaderMenu = (function(superClass) {
  extend(ADAHeaderMenu, superClass);

  function ADAHeaderMenu() {
    return ADAHeaderMenu.__super__.constructor.apply(this, arguments);
  }

  ADAHeaderMenu.prototype.init = function() {
    ADAHeaderMenu.__super__.init.call(this);
    return this.app();
  };

  ADAHeaderMenu.prototype.app = function() {
    var anchorOverride, changeLastWord, getItem, getKeycode, initMapSelector, isDown, isLeft, isRight, isUp, menuState, preventDefault, submenuClose, submenuOpen, toggleDropMenu;
    anchorOverride = void 0;
    changeLastWord = void 0;
    initMapSelector = void 0;
    menuState = void 0;
    toggleDropMenu = function($el) {
      $el = $el || $(document.activeElement);
      if ($el.siblings().hasClass('submenu-content')) {
        $('header nav ul.menu-line > li').removeClass('active');
        $('header nav ul.menu-line > li').find('a').attr('aria-expanded', false);
        $('header nav ul.menu-line > li').find('.submenu-content').attr('aria-hidden', true);
        $el.parent().toggleClass('active');
        $el.attr('aria-expanded', true);
        $el.parent().find('.submenu-content').attr('aria-hidden', false);
      }
    };
    submenuOpen = function($dropdown) {
      $dropdown = $dropdown || $(dropdown);
      $dropdown.addClass('active');
      $dropdown.find('a').attr('aria-expanded', true);
      $dropdown.find('.submenu-content').attr('aria-hidden', false);
    };
    submenuClose = function($el) {
      $el.removeClass('active');
      $el.find('a').attr('aria-expanded', false);
      $el.find('.submenu-content').attr('aria-hidden', true);
    };
    $(window).on('keydown', function(e) {
      var isActive, keycode;
      e.stopPropagation();
      isActive = $(document.activeElement);
      keycode = e.which;
      setTimeout((function() {
        toggleDropMenu();
      }), 300);
    });
    $('header nav ul.menu-line > li').bind('mouseenter', function(e) {
      submenuOpen($(this));
    });
    $('header nav ul.menu-line > li').bind('mouseleave', function(e) {
      submenuClose($(this));
    });
    getKeycode = function(e) {
      return e.keyCode || e.which;
    };
    isLeft = function(keycode) {
      return keycode === 37;
    };
    isRight = function(keycode) {
      return keycode === 39;
    };
    isUp = function(keycode) {
      return keycode === 38;
    };
    isDown = function(keycode) {
      return keycode === 40;
    };
    getItem = function($el, dir) {
      if (dir === null) {
        dir = 'next';
      }
      return $el[dir]();
    };
    preventDefault = function(e) {
      e.stopPropagation();
      return e.preventDefault();
    };
    $.fn.parentAt = function(upTo) {
      var $parent, i;
      $parent = void 0;
      i = void 0;
      i = 0;
      $parent = $(this);
      while (i < upTo) {
        $parent = $parent.parent();
        i++;
      }
      return $parent;
    };
    $('header nav ul.menu-line > li').find('.link-category-section').bind('keydown', function(e) {
      var $el, keycode;
      $el = void 0;
      keycode = void 0;
      $el = $(this);
      keycode = getKeycode(e);
      if (e.altKey || e.ctrlKey) {
        return true;
      }
      if (isLeft(keycode) || isRight(keycode)) {
        return false;
      }
      if (keycode === 27) {
        preventDefault(e);
        submenuClose($(this).parent('li'));
        return false;
      }
      return true;
    });
    $('header nav ul.menu-line > li .submenu-content ol li').bind('keydown', function(e) {
      var $el, $item, dir, keycode;
      $el = void 0;
      $item = void 0;
      dir = void 0;
      keycode = void 0;
      keycode = getKeycode(e);
      $item = $(this).parentAt(3);
      if (isLeft(keycode) || isRight(keycode)) {
        submenuClose($item);
        preventDefault(e);
        if (isLeft(keycode)) {
          $el = $item.prev();
        } else {
          $el = $item.next();
        }
        $el.find('.link-category-section').focus();
        submenuOpen($el);
        return false;
      }
      if (isDown(keycode) || isUp(keycode)) {
        $item = $(this);
        preventDefault(e);
        if (isUp(keycode)) {
          $el = $item.prev();
        } else {
          $el = $item.next();
        }
        $el.find('a').focus();
      }
      if (keycode === 27) {
        preventDefault(e);
        submenuClose($(this).parent('li'));
        setTimeout((function() {
          return $item.find('.link-category-section').focus();
        }), 300);
        return false;
      }
      return true;
    });
  };

  return ADAHeaderMenu;

})(KP.apps.ADABase);

$.kpPluggin(ADAHeaderMenu, 'ADAHeaderMenu', false, false);



},{}],5:[function(require,module,exports){
var ADAImageViewer,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

ADAImageViewer = (function(superClass) {
  extend(ADAImageViewer, superClass);

  function ADAImageViewer() {
    return ADAImageViewer.__super__.constructor.apply(this, arguments);
  }

  ADAImageViewer.prototype.init = function() {
    ADAImageViewer.__super__.init.call(this);
    this.el.dots = this.el.target.find('.imageViewer-dot span');
    this.el.list = this.el.target.find('.imageViewer-list');
    this.el.items = this.el.target.find('.imageViewer-item');
    this.el.next = this.el.target.find('.imageViewer-next');
    this.el.prev = this.el.target.find('.imageViewer-prev');
    this.current = 0;
    setTimeout(((function(_this) {
      return function() {
        return _this.resize();
      };
    })(this)), 200);
    this.events();
    return this.autoPlay = true;
  };

  ADAImageViewer.prototype.buildLabel = function($el, index, text) {
    if (text == null) {
      text = '';
    }
    return $el.attr(this.aria.label, 'page ' + (index + 1) + text);
  };

  ADAImageViewer.prototype.handleDot = function(e, doNotFocus, index) {
    if (index == null) {
      index = null;
    }
    index = index !== null ? index : this.currentTarget(e).index();
    this.current = index;
    if (!doNotFocus) {
      return this.goTo(index);
    }
  };

  ADAImageViewer.prototype.handleDown = function(e) {
    return $(this.el.dots[this.current]).focus();
  };

  ADAImageViewer.prototype.handleUp = function(e) {
    return $(this.el.items[this.current]).focus();
  };

  ADAImageViewer.prototype.handleNext = function(e) {
    this.current = this.handleCurrent();
    return this.handleNextPrev();
  };

  ADAImageViewer.prototype.handlePrev = function(e) {
    var initial;
    initial = this.el.dots.length - 1;
    this.current = this.handleCurrent(-1, initial);
    return this.handleNextPrev();
  };

  ADAImageViewer.prototype.handleNextPrev = function() {
    return $(this.el.dots[this.current]).click();
  };

  ADAImageViewer.prototype.handleCurrent = function(step, initial) {
    if (step == null) {
      step = 1;
    }
    if (initial == null) {
      initial = 0;
    }
    if ((this.current + step) < this.el.dots.length && (this.current + step > -1)) {
      return this.current + step;
    } else {
      return initial;
    }
  };

  ADAImageViewer.prototype.resize = function() {
    var height;
    height = 0;
    this.el.items.each(function() {
      var itemHeight;
      itemHeight = $(this).height();
      if (itemHeight > height) {
        return height = itemHeight;
      }
    });
    if (height) {
      return this.el.list.css('height', height + "px");
    }
  };

  ADAImageViewer.prototype.events = function() {
    var _this;
    _this = this;
    this.el.dots.bind('click', (function(_this) {
      return function(e, doNotFocus) {
        return _this.handleDot(e, doNotFocus);
      };
    })(this));
    this.el.dots.bind('keydown', (function(_this) {
      return function(e) {
        e.stopPropagation();
        if (_this.isEnter(e)) {
          _this.handleDot(e, false);
        }
        if (_this.isUp(e)) {
          _this.fullstop(e);
          return _this.currentTarget(e).click();
        }
      };
    })(this));
    this.el.items.bind('keydown', (function(_this) {
      return function(e) {
        if (_this.isDown(e)) {
          _this.fullstop(e);
          _this.handleDown(e);
        }
        if (_this.isLeft(e)) {
          _this.el.prev.focus();
        }
        if (_this.isRight(e)) {
          return _this.el.next.focus();
        }
      };
    })(this));
    this.el.target.bind('keydown', (function(_this) {
      return function(e) {
        return _this.clearPlay();
      };
    })(this));
    this.el.next.bind('click', (function(_this) {
      return function(e) {
        return _this.handleNext(e);
      };
    })(this));
    this.el.next.bind('keydown', (function(_this) {
      return function(e) {
        if (_this.isLeft(e)) {
          _this.handleUp(e);
        }
        if (_this.isRight(e)) {
          _this.el.prev.focus();
        }
        if (_this.isDown(e)) {
          _this.fullstop(e);
          return _this.handleDown(e);
        }
      };
    })(this));
    this.el.prev.bind('keydown', (function(_this) {
      return function(e) {
        if (_this.isLeft(e)) {
          _this.el.next.focus();
        }
        if (_this.isRight(e)) {
          _this.handleUp(e);
        }
        if (_this.isDown(e)) {
          _this.fullstop(e);
          return _this.handleDown(e);
        }
      };
    })(this));
    return this.el.prev.bind('click', (function(_this) {
      return function(e) {
        return _this.handlePrev(e);
      };
    })(this));
  };

  ADAImageViewer.prototype.startPlay = function() {
    var _this;
    _this = this;
    if (this.autoPlay) {
      return this.intervalId = setInterval(function() {
        _this.current = _this.handleCurrent(1);
        if (!$('html').hasClass('menu-active')) {
          return $(_this.el.dots[_this.current]).trigger('click', false);
        }
      }, 5000);
    }
  };

  ADAImageViewer.prototype.clearPlay = function() {
    this.autoPlay = false;
    return clearInterval(this.intervalId);
  };

  ADAImageViewer.prototype.goTo = function(index) {
    return setTimeout(((function(_this) {
      return function() {
        return $(_this.el.items[index]).find('a').focus();
      };
    })(this)), 200);
  };

  return ADAImageViewer;

})(KP.apps.ADABase);

$.kpPluggin(ADAImageViewer, 'ADAImageViewer');



},{}],6:[function(require,module,exports){
var ADAMobileMenu,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

ADAMobileMenu = (function(superClass) {
  extend(ADAMobileMenu, superClass);

  function ADAMobileMenu() {
    return ADAMobileMenu.__super__.constructor.apply(this, arguments);
  }

  ADAMobileMenu.prototype.init = function() {
    this.sel = {
      container: '.mobile-menu .menu-line'
    };
    this.el = {
      trigger: $('button.mobile-menu'),
      canvas: $('.offcanvas-container'),
      regionSelector: $('.select-region-mobile button'),
      close: $('header button.mobile-menu, nav.mobile-menu .close-mobile-menu'),
      container: $(this.sel.container),
      menu: $(this.sel.container + ' li'),
      button: $(this.sel.container + ' button'),
      submenu: $(this.sel.container + ' ol')
    };
    ADAMobileMenu.__super__.init.call(this);
    this.addListeners();
    return this.setupAttrs();
  };

  ADAMobileMenu.prototype.isOpen = function() {
    return this.el.canvas.hasClass('menu-active');
  };

  ADAMobileMenu.prototype.isSubmenuOpen = function() {
    var _this, open;
    open = false;
    _this = this;
    this.el.submenu.each(function() {
      if ($(this).is(':visible')) {
        open = true;
        return _this.submenu = $(this);
      }
    });
    return open;
  };

  ADAMobileMenu.prototype.focusFirst = function() {
    return this.el.menu.eq(0).find('a:first').focus();
  };

  ADAMobileMenu.prototype.toggleMenu = function(val) {
    this.el.container.parent().attr(this.aria.hidden, val);
    return this.el.trigger.attr(this.aria.expanded, !val);
  };

  ADAMobileMenu.prototype.toggleSubmenuStates = function(e) {
    var expanded, id;
    this._el = this.target(e);
    expanded = JSON.parse(this._el.attr(this.aria.expanded));
    this.el.submenu.attr(this.aria.hidden, true);
    this.el.button.attr(this.aria.expanded, false);
    id = this._el.attr(this.aria.controls);
    this._el.attr(this.aria.expanded, !expanded);
    return $(id).attr(this.aria.hidden, expanded);
  };

  ADAMobileMenu.prototype.addListeners = function() {
    $(window).bind('keydown', (function(_this) {
      return function(e) {
        if (_this.isOpen() && _this.isEsc(e)) {
          if (_this.isSubmenuOpen()) {
            return $('[aria-controls=' + _this.submenu.attr("id") + ']').click();
          } else {
            return _this.el.trigger.click();
          }
        }
      };
    })(this));
    this.el.button.bind('click', (function(_this) {
      return function(e) {
        return _this.toggleSubmenuStates(e);
      };
    })(this));
    this.el.close.bind('click', (function(_this) {
      return function(e) {
        _this.toggleMenu(true);
        return _this.el.trigger.focus();
      };
    })(this));
    this.el.close.bind('keydown', (function(_this) {
      return function(e) {
        if (_this.isEnter(e)) {
          _this.toggleMenu(true);
          return _this.el.trigger.focus();
        }
      };
    })(this));
    this.el.trigger.bind('click', (function(_this) {
      return function(e) {
        return setTimeout((function() {
          if (_this.isOpen()) {
            e.preventDefault();
            _this.toggleMenu(false);
            return _this.focusFirst();
          }
        }), 500);
      };
    })(this));
    this.el.regionSelector.bind('keydown', (function(_this) {
      return function(e) {
        if (_this.isTab(e)) {
          e.preventDefault();
          return _this.el.trigger.focus();
        }
      };
    })(this));
    return this.el.trigger.bind('keydown', (function(_this) {
      return function(e) {
        if (_this.isTab(e) && _this.isOpen()) {
          e.preventDefault();
          return _this.focusFirst();
        }
      };
    })(this));
  };

  ADAMobileMenu.prototype.setupAttrs = function() {
    return this.el.trigger.attr(this.attr.tabindex, 0);
  };

  return ADAMobileMenu;

})(KP.apps.ADABase);

$.kpPluggin(ADAMobileMenu, 'ADAMobileMenu', false, false);



},{}],7:[function(require,module,exports){
var ADARegionSelect,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

ADARegionSelect = (function(superClass) {
  extend(ADARegionSelect, superClass);

  function ADARegionSelect() {
    return ADARegionSelect.__super__.constructor.apply(this, arguments);
  }

  ADARegionSelect.prototype.init = function() {
    ADARegionSelect.__super__.init.call(this);
    this.el.button = this.el.target.find('button');
    this.el.list = this.el.target.find('.js-listbox-ul');
    this.el.listOption = this.el.list.find('a');
    this.el.styleBg = this.el.target.find('.js-presentation-ul');
    this.el.hotspots = this.el.target.find('.link-select-region, .js-presentation-ul, .arrow-bg');
    return this.events();
  };

  ADARegionSelect.prototype.handleButton = function(e) {
    var $el, expanded;
    $el = this.currentTarget(e);
    expanded = $el.attr(this.aria.expanded).toBoolean();
    return this.toggleListBox(e, !expanded);
  };

  ADARegionSelect.prototype.handleMove = function(e, dir) {
    var $el, $li, $list, pos;
    if (dir == null) {
      dir = 'next';
    }
    $el = this.currentTarget(e);
    $li = $el.parent()[dir]();
    if ($li.length) {
      return $li.find('a').focus();
    } else {
      $list = this.el.list.find('li');
      pos = dir === 'next' ? 0 : $list.length - 1;
      return $list.eq(pos).find('a').focus();
    }
  };

  ADARegionSelect.prototype.toggleListBox = function(e, expanded) {
    var fn;
    if (expanded == null) {
      expanded = false;
    }
    fn = expanded ? 'addClass' : 'removeClass';
    this.el.styleBg[fn](this.classes.active);
    this.el.list[fn](this.classes.active);
    this.el.button.attr(this.aria.expanded, expanded);
    return this.el.button.attr(this.aria.controls).byId().attr(this.aria.hidden, !expanded);
  };

  ADARegionSelect.prototype.handleSelection = function(e) {
    var $el, id, url;
    $el = this.currentTarget(e);
    id = $el.data('identifier');
    $.cookie('thriveRegion', id, {
      expires: 7,
      path: '/'
    });
    url = $el.attr('href');
    if (url !== document.location.href) {
      return true;
    }
    return false;
  };

  ADARegionSelect.prototype.focusOptionByIndex = function(pos) {
    return this.el.list.find('li').eq(pos).find('a').focus();
  };

  ADARegionSelect.prototype.events = function() {
    this.el.hotspots.bind('click', (function(_this) {
      return function(e) {
        _this.fullstop(e);
        return _this.el.button.click();
      };
    })(this));
    this.el.listOption.bind('keydown', (function(_this) {
      return function(e) {
        if (_this.isDown(e)) {
          _this.fullstop(e);
          _this.handleMove(e);
        }
        if (_this.isUp(e)) {
          _this.fullstop(e);
          _this.handleMove(e, 'prev');
        }
        if (_this.isEsc(e)) {
          _this.fullstop(e);
          _this.el.button.focus();
        }
        if (_this.isEnter(e)) {
          return _this.handleSelection(e);
        }
      };
    })(this));
    this.el.button.bind('keydown', (function(_this) {
      return function(e) {
        if (_this.isEnter(e)) {
          _this.fullstop(e);
          _this.el.button.click();
        }
        if (_this.isEsc(e)) {
          _this.fullstop(e);
          _this.toggleListBox(e);
        }
        if (_this.isDown(e)) {
          _this.fullstop(e);
          _this.toggleListBox(e, true);
          return _this.focusOptionByIndex(0);
        }
      };
    })(this));
    this.el.button.bind('click', (function(_this) {
      return function(e) {
        _this.fullstop(e);
        _this.handleButton(e);
        return _this.focusOptionByIndex(0);
      };
    })(this));
    return this.el.listOption.bind('click', (function(_this) {
      return function(e) {
        return _this.toggleListBox(e, false);
      };
    })(this));
  };

  return ADARegionSelect;

})(KP.apps.ADABase);

$.kpPluggin(ADARegionSelect, 'ADARegionSelect', true);



},{}],8:[function(require,module,exports){
$.ADA = function() {
  $.ADAHeaderMenu();
  $.ADAHidden();
  $.ADAMobileMenu();
  $.ADATabs();
  $('[data-js-ada-region-select]').ADARegionSelect();
  $('[data-js-ada-image-viewer]').ADAImageViewer();
  return $('[data-js-ada-keydown]').ADAKeydown();
};



},{}],9:[function(require,module,exports){
var ADAHidden,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

ADAHidden = (function(superClass) {
  extend(ADAHidden, superClass);

  function ADAHidden() {
    return ADAHidden.__super__.constructor.apply(this, arguments);
  }

  ADAHidden.prototype.init = function() {
    this.el = {
      target: $('[data-js-ada-hidden]')
    };
    ADAHidden.__super__.init.call(this);
    return this.addListerners();
  };

  ADAHidden.prototype.addListerners = function() {
    var _this;
    _this = this;
    this.parseElements();
    return $(window).resize(function() {
      return _this.parseElements();
    });
  };

  ADAHidden.prototype.parseElements = function() {
    var _this;
    _this = this;
    return this.el.target.each(function() {
      return _this.toggleVisibility({
        el: $(this)
      });
    });
  };

  ADAHidden.prototype.toggleVisibility = function(ops) {
    var asc, size, width;
    this._el = ops.el;
    width = $(window).width();
    asc = this._el.data('asc');
    size = Number(this._el.data('js-ada-hidden'));
    if (asc) {
      if (width >= size) {
        return this.setHidden(true);
      } else {
        return this.setHidden(false);
      }
    } else {
      if (width <= size) {
        return this.setHidden(true);
      } else {
        return this.setHidden(false);
      }
    }
  };

  ADAHidden.prototype.setHidden = function(hidden) {
    return this._el.attr(this.aria.hidden, hidden);
  };

  return ADAHidden;

})(KP.apps.ADABase);

$.kpPluggin(ADAHidden, 'ADAHidden', true, false);



},{}],10:[function(require,module,exports){
var ADAKeydown,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

ADAKeydown = (function(superClass) {
  extend(ADAKeydown, superClass);

  function ADAKeydown() {
    return ADAKeydown.__super__.constructor.apply(this, arguments);
  }

  ADAKeydown.prototype.init = function() {
    ADAKeydown.__super__.init.call(this);
    this.addAttrs();
    return this.events();
  };

  ADAKeydown.prototype.addAttrs = function() {
    var _this;
    _this = this;
    return this.el.target.each(function() {
      var $intent;
      $intent = $(this);
      if (!$intent.attr(_this.attr.tabindex)) {
        return $intent.attr(_this.attr.tabindex, 0);
      }
    });
  };

  ADAKeydown.prototype.handleEvent = function(e) {
    if (this.ops.stopOnEvent) {
      this.fullstop(e);
    }
    return this.currentTarget(e).click();
  };

  ADAKeydown.prototype.events = function() {
    return this.el.target.bind('keydown', (function(_this) {
      return function(e) {
        if (_this.isEnter(e)) {
          _this.handleEvent(e);
        }
        if (_this.ops.addEsc) {
          return _this.handleEvent(e);
        }
      };
    })(this));
  };

  return ADAKeydown;

})(KP.apps.ADABase);

$.kpPluggin(ADAKeydown, 'ADAKeydown', true);



},{}],11:[function(require,module,exports){
var ADANavigation,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

ADANavigation = (function(superClass) {
  extend(ADANavigation, superClass);

  function ADANavigation() {
    return ADANavigation.__super__.constructor.apply(this, arguments);
  }

  ADANavigation.prototype.defaults = {
    submenu: '.dropdown-container',
    submenuItem: '[role="menuitem"]',
    levelTwo: null,
    lastFocusableEl: null,
    afterNavContentEl: null
  };

  ADANavigation.prototype.init = function() {
    ADANavigation.__super__.init.call(this);
    this.el.submenuParents = this.el.target.find(this.ops.submenu);
    this.el.submenus = this.el.submenuParents.find(this.ops.submenuItem);
    this.el.lastFocusableEl = this.ops.lastFocusableEl || this.el.target.find('[aria-haspopup="false"]');
    return this.el.afterNavContentEl = this.ops.afterNavContentEl || $('#kp-main');
  };

  ADANavigation.prototype.toggleClosedLabel = function($el) {
    var state;
    state = this.el.target.data('str-closed') || 'collapsed';
    return this.loopLabels($el, state);
  };

  ADANavigation.prototype.toggleOpenedLabel = function($el) {
    var state;
    state = this.el.target.data('str-opened') || 'expanded';
    return this.loopLabels($el, state);
  };

  ADANavigation.prototype.hasPopup = function($el) {
    return $el.is('[' + this.aria.popup + '="true"]');
  };

  ADANavigation.prototype.loopLabels = function($el, state) {
    var _this;
    _this = this;
    return $el.each(function() {
      if (_this.hasPopup($(this))) {
        return _this.toggleLabel($(this), state);
      }
    });
  };

  ADANavigation.prototype.toggleLabel = function($el, state) {
    var label;
    label = $el.data('label');
    return $el.attr(this.aria.label, label + ' ' + state);
  };

  ADANavigation.prototype.handleLastFocusableEl = function() {
    return this.el.lastFocusableEl.bind('keydown', (function(_this) {
      return function(e) {
        if (_this.isDown(e) && (_this.currentTarget(e).attr(_this.aria.popup) !== 'true')) {
          return _this.el.afterNavContentEl.focus();
        }
      };
    })(this));
  };

  ADANavigation.prototype.handleSubmenuItemMove = function(e, index) {
    var $container, $focus, $list, $submenu, lastItem, move, total;
    $submenu = this.currentTarget(e).parents(this.ops.submenu);
    $container = this.currentTarget(e).parents(this.ops.columns);
    $list = $submenu.find('li');
    total = $list.length - 1;
    if (this.isUp(e)) {
      move = -1;
      if (index === 0) {
        index = total;
      } else {
        --index;
      }
    } else {
      move = 1;
      if (index === total) {
        lastItem = true;
        index = 0;
      } else {
        ++index;
      }
    }
    $focus = $list.eq(index);
    if (this.ops.levelTwo) {
      if ($focus.hasClass(this.ops.levelTwo)) {
        $focus = $list.eq(index + move);
      }
    }
    if ($focus.find('ul').length) {
      $focus = $submenu.find('li ul li').eq(0);
    }
    if (!(!this.ops.wrapFocus && lastItem)) {
      $focus.find('a').focus();
    }
    return false;
  };

  ADANavigation.prototype.handleSubmenu = function(ops) {
    var _this;
    $.extend(this.ops, ops);
    _this = this;
    return this.el.submenuParents.each(function() {
      var $list;
      $list = $(this).find('li');
      return $.each($list, function(index, value) {
        var $item;
        $item = $list.eq(index).find('a');
        if ($item.data('vertical')) {
          $item.unbind('keydown');
        }
        $item.data('vertical', true);
        return $item.bind('keydown', function(e) {
          if (_this.isDown(e) || _this.isUp(e)) {
            _this.fullstop(e);
            _this.handleSubmenuItemMove(e, index);
            return false;
          }
        });
      });
    });
  };

  return ADANavigation;

})(KP.apps.ADABase);

$.kpPluggin(ADANavigation, 'ADANavigation', false);



},{}],12:[function(require,module,exports){

/*
  @depends: helpers/common/string.prototype.coffee
 */
var ADAPopup,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

ADAPopup = (function(superClass) {
  extend(ADAPopup, superClass);

  function ADAPopup() {
    return ADAPopup.__super__.constructor.apply(this, arguments);
  }

  ADAPopup.prototype.defaults = {
    tabable: {
      first: null,
      last: null
    }
  };

  ADAPopup.prototype.init = function() {
    var $container;
    ADAPopup.__super__.init.call(this);
    this.sel = {
      close: '.js-close',
      popup: '#js-popup',
      title: '.js-popup-title',
      content: '.js-popup-content',
      contentFirst: '.js-focus-popup-content-first',
      contentLast: '.js-focus-popup-content-last'
    };
    this.sel = $.extend(this.sel, this.ops.sel);
    $container = this.getContainer();
    this.el.close = $container.find(this.sel.close);
    this.el.content = $container.find(this.sel.content);
    this.el.title = $container.find(this.sel.title);
    this.el.contentFirstFocus = this.el.content.find(this.sel.contentFirst);
    this.el.contentLastFocus = this.el.content.find(this.sel.contentLast);
    return this.events();
  };

  ADAPopup.prototype.getTabable = function(key) {
    var $tabable;
    if (key == null) {
      key = 'first';
    }
    if (this.ops.tabable[key]) {
      $tabable = this.ops.tabable[key];
    } else {
      $tabable = this.getContainer().find('a:' + key);
    }
    if (!$tabable.length) {
      $tabable = this.getContainer().find('[tabindex!=-1]:' + key);
    }
    return $tabable;
  };

  ADAPopup.prototype.getContainer = function(e) {
    var $el, id;
    $el = this.getTarget(e);
    id = $el.attr(this.aria.controls);
    if (id) {
      return $el.attr(this.aria.controls).byId();
    } else {
      return $(this.sel.popup);
    }
  };

  ADAPopup.prototype.getTarget = function(e) {
    if (e) {
      return this.currentTarget(e);
    } else {
      return this.el.target;
    }
  };

  ADAPopup.prototype.toggle = function(e) {
    var $container, $el;
    $el = this.getTarget(e);
    this.expanded = !($el.attr(this.aria.expanded) === 'true' ? true : false);
    $el.attr(this.aria.expanded, this.expanded);
    $container = this.getContainer();
    if ($container.length) {
      return $container.attr(this.aria.hidden, !this.expanded);
    }
  };

  ADAPopup.prototype.isExpanded = function(e) {
    var $el;
    $el = this.getTarget(e);
    if ($el.attr(this.aria.expanded) === 'true') {
      return true;
    } else {
      return false;
    }
  };

  ADAPopup.prototype.handleFocus = function(e, key, fn) {
    fn = fn || this.isTab;
    if (fn(e) && this.isExpanded()) {
      this.fullstop(e);
      return this.getTabable(key).focus();
    }
  };

  ADAPopup.prototype.events = function() {
    this.el.target.click((function(_this) {
      return function(e) {
        if (!_this.expanded || !_this.el.close.length) {
          return _this.toggle(e);
        }
      };
    })(this));
    this.el.close.click((function(_this) {
      return function(e) {
        _this.toggle();
        return _this.el.target.focus();
      };
    })(this));
    this.el.close.keydown((function(_this) {
      return function(e) {
        if (_this.isEnter(e)) {
          return _this.el.close.click();
        }
      };
    })(this));
    this.el.target.keydown((function(_this) {
      return function(e) {
        if (_this.isTab(e) && _this.expanded) {
          return _this.getContainer().focus();
        }
      };
    })(this));
    this.getTabable('last').keydown((function(_this) {
      return function(e) {
        if (_this.isDown(e)) {
          return _this.fullstop(e);
        } else {
          return _this.handleFocus(e, 'first');
        }
      };
    })(this));
    this.getTabable('first').keydown((function(_this) {
      return function(e) {
        if (_this.isDown(e) && _this.el.title.length) {
          _this.fullstop(e);
          return _this.el.title.focus();
        } else {
          return _this.handleFocus(e, 'last', _this.isBackTab);
        }
      };
    })(this));
    this.el.title.keydown((function(_this) {
      return function(e) {
        if (_this.isUp(e)) {
          _this.getTabable('first').focus();
        }
        if (_this.isDown(e)) {
          _this.fullstop(e);
          return _this.el.content.focus();
        }
      };
    })(this));
    this.el.content.keydown((function(_this) {
      return function(e) {
        if (_this.isDown(e)) {
          _this.fullstop(e);
          return _this.el.contentFirstFocus.focus();
        }
      };
    })(this));
    this.el.contentFirstFocus.keydown((function(_this) {
      return function(e) {
        if (_this.isUp(e)) {
          return _this.el.content.focus();
        }
      };
    })(this));
    return this.el.contentLastFocus.keydown((function(_this) {
      return function(e) {
        if (_this.isDown(e)) {
          _this.fullstop(e);
          return _this.el.close.focus();
        }
      };
    })(this));
  };

  return ADAPopup;

})(KP.apps.ADABase);

$.kpPluggin(ADAPopup, 'ADAPopup', true);



},{}],13:[function(require,module,exports){
var ADATabs,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

ADATabs = (function(superClass) {
  extend(ADATabs, superClass);

  function ADATabs() {
    return ADATabs.__super__.constructor.apply(this, arguments);
  }

  ADATabs.prototype.init = function() {
    this.el = {
      ctrl: $('[data-js-tab]'),
      tabpanel: $('[role="tabpanel"]'),
      tab: $('[role="tab"]')
    };
    ADATabs.__super__.init.call(this);
    this.classes.vertical = 'vertical';
    return this.events();
  };

  ADATabs.prototype.events = function() {
    return this.tabs();
  };

  ADATabs.prototype.getAuto = function($el) {
    if ($el.is('[data-auto-open]')) {
      return JSON.parse($el.data('auto-open'));
    } else {
      return true;
    }
  };

  ADATabs.prototype.handleTabs = function(e, vertical) {
    var $_active, $focus, $parent, find, keycode, next, prev;
    if (vertical == null) {
      vertical = false;
    }
    keycode = this.keycode(e);
    if (vertical) {
      prev = 'up';
      next = 'down';
    } else {
      prev = 'left';
      next = 'right';
    }
    if (this.tab.prev().length || this.tab.next().length) {
      $parent = this.tab;
      find = false;
    } else {
      $parent = this.tab.parent();
      find = true;
    }
    $_active = $parent;
    if (keycode === this.keys[prev] && $parent.prev) {
      $_active = $parent.prev();
    }
    if (keycode === this.keys[next] && $parent.next) {
      $_active = $parent.next();
    }
    $focus = find ? $_active.find(this.tab.prop('tagName')) : $_active;
    if (!this.getAuto($focus)) {
      $focus.attr(this.attr.tabindex, 0).focus();
    }
    return setTimeout(((function(_this) {
      return function() {
        if (_this.getAuto($focus)) {
          $focus.click();
          if (!(vertical && _this.isRight(e))) {
            return $focus.focus();
          }
        }
      };
    })(this)), 300);
  };

  ADATabs.prototype.tabs = function() {
    var _this;
    _this = this;
    this.el.tab.bind('click', (function(_this) {
      return function(e) {
        _this.tab = _this.currentTarget(e);
        return _this.toggleTabsStates(e);
      };
    })(this));
    this.el.tab.bind('keydown', function(e) {
      var vertical;
      _this.tab = $(this);
      vertical = _this.tab.hasClass(_this.classes.vertical) || _this.tab.parent().hasClass(_this.classes.vertical);
      if (_this.isSpace(e) || _this.isEnter(e)) {
        e.preventDefault();
        if (!_this.getAuto(_this.tab)) {
          _this.tab.click();
        }
        return;
      }
      if (!_this.isTab(e)) {
        return _this.handleTabs(e, vertical);
      }
    });
    this.el.ctrl.on('click', function(e) {
      e.preventDefault();
      return $($(this).attr('data-js-tab')).click();
    });
    return this.el.ctrl.on('keydown', (function(_this) {
      return function(e) {
        if (_this.isEnter(e)) {
          return _this.target(e).click();
        }
      };
    })(this));
  };

  ADATabs.prototype.focusTabpanel = function(e) {
    var id;
    id = this.currentTarget(e).attr('id');
    return $('[aria-labelledby=' + id + ']').focus();
  };

  ADATabs.prototype.toggleTabsStates = function(e) {
    var $el, id, selector;
    $el = this.tab;
    this.el.tab.attr(this.aria.tabindex, -1).attr(this.aria.selected, false);
    this.el.tabpanel.attr(this.aria.hidden, true).attr(this.aria.tabindex, -1);
    $el.attr(this.aria.tabindex, 0).attr(this.aria.selected, true);
    id = $el.attr('id');
    selector = '[aria-labelledby="' + id + '"]';
    return $(selector).attr(this.aria.hidden, false).attr(this.aria.tabindex, 0);
  };

  return ADATabs;

})(KP.apps.ADABase);

$.kpPluggin(ADATabs, 'ADATabs', true, false);



},{}],14:[function(require,module,exports){
var ADABase,
  bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

ADABase = (function(superClass) {
  extend(ADABase, superClass);

  function ADABase() {
    this.triggerClick = bind(this.triggerClick, this);
    this.fullstop = bind(this.fullstop, this);
    this.keycode = bind(this.keycode, this);
    this.isEnterOrSpace = bind(this.isEnterOrSpace, this);
    this.isEsc = bind(this.isEsc, this);
    this.isBackTab = bind(this.isBackTab, this);
    this.isTab = bind(this.isTab, this);
    this.isSpace = bind(this.isSpace, this);
    this.isEnter = bind(this.isEnter, this);
    this.isDown = bind(this.isDown, this);
    this.isUp = bind(this.isUp, this);
    this.isLeft = bind(this.isLeft, this);
    this.isRight = bind(this.isRight, this);
    return ADABase.__super__.constructor.apply(this, arguments);
  }

  ADABase.prototype.init = function() {
    this.keys = {
      esc: 27,
      down: 40,
      up: 38,
      left: 37,
      right: 39,
      o: 79,
      space: 32,
      tab: 9,
      enter: 13
    };
    this.aria = {
      tabindex: 'tabindex',
      selected: 'aria-selected',
      invalid: 'aria-invalid',
      pressed: 'aria-pressed',
      checked: 'aria-checked',
      owns: 'aria-owns'
    };
    return ADABase.__super__.init.call(this);
  };

  ADABase.prototype.isRight = function(e) {
    return this.keycode(e) === this.keys.right;
  };

  ADABase.prototype.isLeft = function(e) {
    return this.keycode(e) === this.keys.left;
  };

  ADABase.prototype.isUp = function(e) {
    return this.keycode(e) === this.keys.up;
  };

  ADABase.prototype.isDown = function(e) {
    return this.keycode(e) === this.keys.down;
  };

  ADABase.prototype.isEnter = function(e) {
    return this.keycode(e) === this.keys.enter;
  };

  ADABase.prototype.isSpace = function(e) {
    return this.keycode(e) === this.keys.space;
  };

  ADABase.prototype.isTab = function(e) {
    return this.keycode(e) === this.keys.tab;
  };

  ADABase.prototype.isBackTab = function(e) {
    return e.shiftKey && this.isTab(e);
  };

  ADABase.prototype.isEsc = function(e) {
    return this.keycode(e) === this.keys.esc;
  };

  ADABase.prototype.isEnterOrSpace = function(e) {
    return this.isEnter(e) || this.isSpace(e);
  };

  ADABase.prototype.keycode = function(e) {
    return e.which;
  };

  ADABase.prototype.fullstop = function(e) {
    e.stopPropagation();
    return e.preventDefault();
  };

  ADABase.prototype.triggerClick = function(e, $el) {
    $el = $el || this.currentTarget(e);
    if (this.isEnterOrSpace(e)) {
      return $el.click();
    }
  };

  return ADABase;

})(KP.apps.KPModule);

$.kpInit(ADABase, 'ADABase');



},{}],15:[function(require,module,exports){
var AjaxContent,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

AjaxContent = (function(superClass) {
  extend(AjaxContent, superClass);

  function AjaxContent() {
    return AjaxContent.__super__.constructor.apply(this, arguments);
  }

  AjaxContent.prototype.defaults = {
    method: 'GET',
    type: 'json',
    data: null,
    wrapperHtmlOpen: '',
    wrapperHtmlClose: '',
    sel: {
      container: '[role="main"].content-wrapper'
    }
  };

  AjaxContent.prototype.init = function() {
    return this.ajax();
  };

  AjaxContent.prototype.getMethod = function() {
    return this.el.target.data('method') || this.ops.method;
  };

  AjaxContent.prototype.getUrl = function() {
    if (!this.ops.opsOverHtml) {
      return this.el.target.data('href') || this.ops.url;
    } else {
      return this.ops.url;
    }
  };

  AjaxContent.prototype.getContainerSel = function() {
    return this.el.target.data('container') || this.ops.sel.container;
  };

  AjaxContent.prototype.ajax = function() {
    var ops;
    ops = {
      method: this.getMethod(),
      url: this.getUrl()
    };
    if (this.ops.type) {
      ops.dataType = this.ops.type;
    }
    if (this.ops.data) {
      ops.data = this.ops.data;
    }
    return $.ajax(ops).error((function(_this) {
      return function(e, xt, xs) {
        return console.error(e);
      };
    })(this)).done((function(_this) {
      return function(html) {
        if (_this.ops.afterAjax) {
          _this.ops.afterAjax(html);
        } else {
          $(_this.getContainerSel()).html(_this.ops.wrapperHtmlOpen + html + _this.ops.wrapperHtmlClose);
          $(_this.getContainerSel()).removeAttr('style');
        }
        if (!_this.ops.noJsReload) {
          $.loadPluggins({
            ajax: true
          });
        }
      };
    })(this));
  };

  return AjaxContent;

})(KP.apps.KPModule);

$.kpPluggin(AjaxContent, 'AjaxContent', true);



},{}],16:[function(require,module,exports){
var Footnote,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

Footnote = (function(superClass) {
  extend(Footnote, superClass);

  function Footnote() {
    return Footnote.__super__.constructor.apply(this, arguments);
  }

  Footnote.prototype.init = function() {
    this.el = {
      footnotes: $("[data-js-footnote]"),
      listfootnotes: $(".list-footnote")
    };
    return this.render();
  };

  Footnote.prototype.render = function() {
    var footnote, i, j, len, len1, ref, ref1, results, that;
    that = this;
    ref = this.el.footnotes;
    for (i = 0, len = ref.length; i < len; i++) {
      footnote = ref[i];
      $(footnote).find('ul li').each(function() {
        var $el, escaped, html, note, num, nums;
        $el = $(this);
        html = $el.html();
        nums = html.match(/^\d+/);
        if (nums) {
          num = nums[0];
          note = html.substr(num.length);
        } else {
          note = html;
        }
        escaped = note.match(/^#\d+/);
        if (escaped) {
          note = note.substr(1);
        }
        note = '<span class="footnote-info meta">' + note + '</span>';
        num = num || $el.index() + 1;
        num = ("" + num).sup();
        $el.html(num + note);
        return that.style($el.find('sup'), $el);
      });
    }
    ref1 = this.el.listfootnotes;
    results = [];
    for (j = 0, len1 = ref1.length; j < len1; j++) {
      footnote = ref1[j];
      results.push($(footnote).find('ul li').each(function() {
        return that.style($(this).find('sup'), $(this));
      }));
    }
    return results;
  };

  Footnote.prototype.style = function($sup, $el) {
    var $first;
    $first = $sup.first();
    $first.addClass("footnote-num meta");
    if ($first.text().length > 2) {
      return $el.parent().addClass("has-large-num");
    }
  };

  return Footnote;

})(KP.apps.KPModule);

$.kpPluggin(Footnote, 'Footnote', true, false);



},{}],17:[function(require,module,exports){

/*
  @depends:
    helpers/common/string.prototype.coffee
    modules/ada/common/_ada-navigation.coffee
 */
var GeoLocation,
  bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

GeoLocation = (function(superClass) {
  extend(GeoLocation, superClass);

  function GeoLocation() {
    this.geoError = bind(this.geoError, this);
    this.geoSuccess = bind(this.geoSuccess, this);
    this.saved = bind(this.saved, this);
    this.afterPopup = bind(this.afterPopup, this);
    return GeoLocation.__super__.constructor.apply(this, arguments);
  }

  GeoLocation.prototype.init = function() {
    var $target;
    $target = this.el.target;
    this.el = {
      geolocationRequired: $('.js-geolocation-required'),
      locationIcon: $('.js-location-icon'),
      regionName: $('.js-region-name'),
      lang: $('html').attr('lang'),
      target: $target,
      mapContentList: $('.js-map-content-list'),
      mapRegionList: $('.js-map-region-list'),
      srcRegion: $('.js-src-region').data('src-region'),
      shopKp: $('[data-shopkp]'),
      trackingGeoYes: $('.js-geo-detection-yes')[0],
      trackingGeoNo: $('.js-geo-detection-no')[0]
    };
    this.sel = {
      popupRegionSelect: '[data-js-region-select] li a',
      mapContent: '.map-content',
      selectedRegionName: '.js-selected-region-name'
    };
    GeoLocation.__super__.init.call(this);
    this.classes.popup = 'js-popup';
    this.classes.hidden = 'hidden';
    this.classes.active = 'is-active';
    this.events();
    this.setUpData();
    this.getUserLocation();
    if ($.trim(this.el.srcRegion)) {
      this.el.mapRegionList.hide();
      this.el.mapContentList.removeClass(this.classes.hidden);
      return this.setVisibleMapContent(this.el.srcRegion);
    }
  };

  GeoLocation.prototype.goTo = function(e) {
    var href;
    href = this.currentTarget(e).attr('href');
    return window.location = href;
  };

  GeoLocation.prototype.openPopup = function(e) {
    var $popup;
    $popup = this.currentTarget(e);
    if (this.userLocation && ($popup.data('type') === "doctor" || $popup.data('type') === "facility")) {
      return this.goTo(e);
    } else {
      return $popup.Popup({
        action: 'SelectRegion',
        apiKey: 'select_region',
        linkType: $popup.data('type'),
        srcPage: this.el.target.data('js-geolocation'),
        callback: this.afterPopup
      });
    }
  };

  GeoLocation.prototype.manualSelection = function(e) {
    var key;
    key = this.currentTarget(e).data('region-code');
    this.updateSelectedRegionText(key);
    return this.goTo(e);
  };

  GeoLocation.prototype.afterPopup = function() {
    var $popup;
    $(this.sel.popupRegionSelect).bind('click', (function(_this) {
      return function(e) {
        return _this.manualSelection(e);
      };
    })(this));
    $popup = this.classes.popup.byId();
    $popup.ADANavigation({
      submenu: 'ul',
      submenuItem: 'li'
    });
    return $popup.data('kp-ADANavigation').handleSubmenu({
      wrapFocus: false
    });
  };

  GeoLocation.prototype.events = function() {
    return this.el.geolocationRequired.bind('click', (function(_this) {
      return function(e) {
        e.preventDefault();
        return _this.openPopup(e);
      };
    })(this));
  };

  GeoLocation.prototype.calculateDistanceBetween = function(p1, p2) {
    var RADIUS, a, angularDistance, dLat, dLong, rad;
    rad = function(x) {
      return x * Math.PI / 180;
    };
    RADIUS = 6371;
    dLat = rad(p2.lat - p1.lat);
    dLong = rad(p2.lng - p1.lng);
    a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(rad(p1.lat)) * Math.cos(rad(p2.lat)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
    angularDistance = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return RADIUS * angularDistance;
  };

  GeoLocation.prototype.setUpData = function() {
    if (this.ops.regions) {
      return this.regions = this.ops.regions;
    } else {
      return this.regions = {
        "NCA": this.createPoint(38.8375, -120.8958),
        "SCA": this.createPoint(34.9592, -116.4194),
        "CO": this.createPoint(39.5501, -105.7821),
        "GA": this.createPoint(32.1656, -82.9001),
        "HAW": this.createPoint(19.8968, -155.5828),
        "NW": this.createPoint(38.938091, -77.044933),
        "MID": this.createPoint(39.0458, -76.6413)
      };
    }
  };

  GeoLocation.prototype.createPoint = function(lat, lng) {
    return {
      lat: lat,
      lng: lng
    };
  };

  GeoLocation.prototype.getUserLocation = function() {
    var options;
    options = {
      timeout: 10000,
      maximumAge: 0
    };
    if (this.el.target.data('geo-disabled')) {
      return;
    }
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(this.geoSuccess, this.geoError, options);
      return setTimeout(((function(_this) {
        return function() {
          if (!_this.userLocation) {
            return _this.geoError();
          }
        };
      })(this)), 5000);
    }
  };

  GeoLocation.prototype.findRegionName = function() {
    var distance, index, name, regionNameIndex, shortestDistance, sortedDistances, unsortedDistances, userLocation;
    unsortedDistances = [];
    sortedDistances = [];
    userLocation = {
      lat: this.userLocation.latitude,
      lng: this.userLocation.longitude
    };
    for (name in this.regions) {
      distance = this.calculateDistanceBetween(this.regions[name], userLocation);
      unsortedDistances.push(distance);
      sortedDistances.push(distance);
    }
    sortedDistances = sortedDistances.sort();
    if (sortedDistances.length) {
      shortestDistance = sortedDistances[0];
      regionNameIndex = unsortedDistances.indexOf(shortestDistance);
      index = 0;
      for (name in this.regions) {
        if (index === regionNameIndex) {
          this.regionNameKey = name;
        }
        index++;
      }
      if ($.kpAPI('regions')) {
        this.updateSelectedRegionText();
      }
      return this.saveInformation();
    } else {
      return console.error('Unexpect error with geolocations...');
    }
  };

  GeoLocation.prototype.saved = function(resp) {
    var key, name;
    key = 'name';
    if (resp.doctor) {
      this.updateAttr(resp, 'doctor');
    }
    if (resp.facility) {
      this.updateAttr(resp, 'facility');
    }
    if (this.el.lang === "es") {
      key = key + '_es';
    }
    name = resp[key];
    this.el.regionName.html(name);
    this.setVisibleMapContent(name);
    if (this.el.shopKp.length) {
      if (name === "Southern California" || name === "Sur de California") {
        return window.location = this.el.shopKp.attr('data-scal-shopkp');
      } else {
        return window.location = this.el.shopKp.attr('data-ncal-shopkp');
      }
    }
  };

  GeoLocation.prototype.setVisibleMapContent = function(key, attr) {
    var name;
    if (attr == null) {
      attr = 'id';
    }
    key = key || this.el.mapContentList.find('select').val();
    if (this.el.mapContentList.length) {
      name = key ? key : (this.regionNameKey ? this.getRegionNameByKey(this.regionNameKey) : void 0);
      this.el.mapContentList.find(this.sel.selectedRegionName).html(name);
      this.el.mapContentList.find(this.sel.mapContent).hide();
      return this.el.mapContentList.find('[data-' + attr + '="' + key + '"]').show();
    }
  };

  GeoLocation.prototype.updateAttr = function(data, key, attr) {
    if (attr == null) {
      attr = 'href';
    }
    return $('a[data-type="' + key + '"]').attr(attr, data[key]);
  };

  GeoLocation.prototype.updateSelectedRegionText = function(key) {
    var name;
    key = key || this.regionNameKey;
    name = this.getRegionNameByKey(key);
    this.el.regionName.text(name);
    return this.setVisibleMapContent(name);
  };

  GeoLocation.prototype.getRegionNameByKey = function(key) {
    return $.kpAPI(key, $.kpAPI('regions'));
  };

  GeoLocation.prototype.saveInformation = function() {
    var name;
    name = this.regionNameKey;
    return $('body').AjaxContent({
      data: {
        action: "GeoLocation",
        userLocation: {
          lat: this.userLocation.latitude,
          lng: this.userLocation.longitude
        },
        regionName: name,
        regionLocation: this.regions[name]
      },
      method: "POST",
      noJsReload: true,
      url: $.kpAPI('geolocation') || "/wp-admin/admin-ajax.php",
      afterAjax: this.saved
    });
  };

  GeoLocation.prototype.geoSuccess = function(position) {
    this.userLocation = position.coords;
    this.el.locationIcon.addClass(this.classes.active);
    this.el.geolocationRequired.removeAttr(this.aria.popup).removeAttr(this.aria.controls);
    this.findRegionName();
    this.el.mapRegionList.hide();
    this.el.mapContentList.removeClass(this.classes.hidden);
    if (this.el.trackingGeoYes.length) {
      return this.el.trackingGeoYes.click();
    }
  };

  GeoLocation.prototype.geoError = function() {
    console.log('You did not share your location...');
    if (!$(this.classes.popup).length) {
      $('body').append("<div id='js-popup'><!-- Empty popup container --></div>");
    }
    this.el.geolocationRequired.attr(this.aria.popup, true).attr(this.aria.controls, this.classes.popup);
    this.el.mapRegionList.show();
    this.el.mapContentList.addClass(this.classes.hidden);
    if (this.el.shopKp.length) {
      window.location = this.el.shopKp.attr('data-ncal-shopkp');
    }
    if (this.el.trackingGeoNo.length) {
      return this.el.trackingGeoNo.click();
    }
  };

  return GeoLocation;

})(KP.apps.KPModule);

$.kpPluggin(GeoLocation, 'GeoLocation');



},{}],18:[function(require,module,exports){
var KPModule;

KPModule = (function() {
  KPModule.prototype.defaults = {};

  function KPModule(options, element) {
    this.ops = $.extend({}, this.defaults, options);
    this.el = this.el || {};
    this.sel = this.sel || {};
    this.el.target = $(element);
    this.init();
  }

  KPModule.prototype.init = function() {
    this.classes = {
      toggle: 'toggle',
      active: 'active',
      inactive: 'inactive',
      fade: 'fade',
      offscreen: 'l-offscreen',
      selected: 'selected',
      open: 'open',
      close: 'close'
    };
    this.aria = this.aria || {};
    this.aria.label = 'aria-label';
    this.aria.controls = 'aria-controls';
    this.aria.described = 'aria-describedby';
    this.aria.labelled = 'aria-labelledby';
    this.aria.popup = 'aria-haspopup';
    this.aria.expanded = 'aria-expanded';
    this.aria.hidden = 'aria-hidden';
    return this.attr = {
      tabindex: 'tabindex'
    };
  };

  KPModule.prototype.target = function(e) {
    return $(e.target);
  };

  KPModule.prototype.currentTarget = function(e) {
    return $(e.currentTarget);
  };

  KPModule.prototype.isSet = function(obj) {
    return typeof obj !== 'undefined';
  };

  return KPModule;

})();

$.kpInit(KPModule, 'KPModule');



},{}],19:[function(require,module,exports){
var Popup,
  bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

Popup = (function(superClass) {
  extend(Popup, superClass);

  function Popup() {
    this.openPopup = bind(this.openPopup, this);
    return Popup.__super__.constructor.apply(this, arguments);
  }

  Popup.prototype.defaults = {
    theme: 'theme-light'
  };

  Popup.prototype.init = function() {
    Popup.__super__.init.call(this);
    this.sel = {
      close: '.js-popup-btn-close',
      popup: '.m-popup',
      popupId: '#js-popup',
      overlay: '.popup-overlay',
      content: '.js-popup-content',
      okay: '.js-popup-btn-okay',
      lastFocusableElement: '.js-focus-last'
    };
    this.classes.popupActive = 'popup-is-active';
    return this.initPopup();
  };

  Popup.prototype.initPopup = function() {
    if (!$(this.sel.popupId).length) {
      $('body').append("<div id='js-popup'><!-- Empty popup container --></div>");
    }
    return $('body').AjaxContent({
      data: {
        action: this.ops.action,
        linkType: this.ops.linkType,
        srcPage: this.ops.srcPage
      },
      type: 'html',
      noJsReload: true,
      method: this.ops.method || "POST",
      url: $.kpAPI(this.ops.apiKey, $.kpAPI('popup')) || "/wp-admin/admin-ajax.php",
      afterAjax: this.openPopup
    });
  };

  Popup.prototype.openPopup = function(html) {
    $(this.sel.popupId).remove();
    $('body').append(html);
    if (!this.ops.noOverlay) {
      $('body').append('<div class="popup-overlay ' + this.ops.theme + '"></div>');
      $(this.sel.overlay).show();
    }
    $(this.sel.popup).addClass(this.classes.popupActive).addClass(this.classes.popupActive).attr('id', 'js-popup').attr(this.aria.hidden, false);
    setTimeout(((function(_this) {
      return function() {
        if (_this.ops.callback) {
          _this.ops.callback();
        }
        return $(_this.sel.close).focus();
      };
    })(this)), 1000);
    this.el.target.attr(this.aria.expanded, true);
    $('body').addClass(this.classes.popupActive);
    return this.events();
  };

  Popup.prototype.events = function() {
    var $last;
    $(this.sel.close).bind('click', (function(_this) {
      return function(e) {
        e.preventDefault();
        return _this.closePopup(e);
      };
    })(this));
    if ($(this.sel.okay).length) {
      $last = $(this.sel.okay);
    }
    return this.el.target.ADAPopup({
      tabable: {
        first: $(this.sel.close),
        last: $last || $(this.sel.lastFocusableElement)
      },
      sel: {
        close: this.sel.close
      }
    });
  };

  Popup.prototype.closePopup = function() {
    $('body').removeClass(this.classes.popupActive);
    $(this.sel.popup).html('').removeClass(this.classes.popupActive);
    return $(this.sel.overlay).remove();
  };

  return Popup;

})(KP.apps.KPModule);

$.kpPluggin(Popup, 'Popup', true);



},{}],20:[function(require,module,exports){
var Tabs,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

Tabs = (function(superClass) {
  extend(Tabs, superClass);

  function Tabs() {
    return Tabs.__super__.constructor.apply(this, arguments);
  }

  Tabs.prototype.init = function() {
    this.el = {
      tab: $('[role="tab"]')
    };
    Tabs.__super__.init.call(this);
    return this.events();
  };

  Tabs.prototype.handle = function(e) {
    var $el, id;
    $el = this.currentTarget(e);
    id = $el.attr('id');
    $el.addClass(this.classes.active).removeClass(this.classes.inactive).siblings().removeClass(this.classes.active).addClass(this.classes.inactive);
    $('[aria-labelledby=' + id + ']').addClass(this.classes.active).removeClass(this.classes.inactive).siblings().removeClass(this.classes.active).addClass(this.classes.inactive);
    $('[data-parent-of=' + id + ']').addClass(this.classes.active).removeClass(this.classes.inactive).siblings().removeClass(this.classes.active).addClass(this.classes.inactive);
    return $('[data-parent-of=' + id + ']').addClass(this.classes.active).siblings().removeClass(this.classes.active);
  };

  Tabs.prototype.events = function() {
    return this.el.tab.bind('click', (function(_this) {
      return function(e) {
        e.preventDefault();
        return _this.handle(e);
      };
    })(this));
  };

  return Tabs;

})(KP.apps.KPModule);

$.kpPluggin(Tabs, 'Tabs', true, false);



},{}],21:[function(require,module,exports){
$(document).ready(function() {
  $.loadPluggins = function() {
    $.ADA();
    return $('[data-js-geolocation]').each(function() {
      var regions;
      regions = $(this).data('js-geolocation');
      if (!regions || (typeof regions === "string")) {
        return $(this).GeoLocation();
      } else {
        return $(this).GeoLocation({
          regions: regions
        });
      }
    });
  };
  $.Tabs();
  if ($("[data-js-footnote]")) {
    $.Footnote();
  }
  return $.loadPluggins();
});

$(window).on('pageshow', function(event) {
  if (event.originalEvent.persisted) {
    window.location.reload();
  }
});



},{}]},{},[3])